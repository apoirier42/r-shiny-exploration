![](shiny.S.png)

## Explore Shiny possibilities and challenges

The goal here is to create small web applications with [Shiny](https://shiny.rstudio.com/) to explore and understand various possibilities and challenges.

### Parallel execution

Use [promises](https://rstudio.github.io/promises/) package for parallel execution (*async* programming).

See [parallel-execution-future-promises](parallel-execution-future-promises) directory.
