#' Download a file in a process in parallel, so another user (another session)
#' can still interact with the app.
#'
library(shiny)
library(future)

data <- matrix(rnorm(3e6), ncol = 10)

ui <- fluidPage(
  tags$head(tags$style(type='text/css', 'body {background-color: #272b30; color: lightgray;}')),

  tags$h4('Parallel processing benefits can be observed by using 2 different sessions
           (open this app in 2 different windows)'),

  selectInput('plan1', 'Processing:', choices = c('multicore', 'sequential'), selected = 'multicore'),

  downloadButton('downloadData', 'Download'), tags$br(), tags$br(),

  actionButton('newRandomNum', 'Click for a random number'), tags$br(), tags$br(),

  textOutput('number')
)

server <- function(input, output, session) {

  output$downloadData <- downloadHandler(
    filename = function() { paste0('data-', Sys.Date(), '.csv')},
    content = function(file) {future({write.csv(data, file)})} #  <---  ---  ---  ---  ---  ---
  )

  output$number <- renderText({trigger <- input$newRandomNum
                               as.character(rnorm(1, 42, 11111))})

  observeEvent(input$plan1, {
    fn <- match.fun(input$plan1)
    plan(fn)
  })
}

shinyApp(ui = ui, server = server)
