#'
#' Keep user preferences in HTML5 localStorage
#' https://shiny.rstudio.com/articles/js-send-message.html
#' https://stackoverflow.com/questions/50758173/access-javascript-variable-in-r-shiny-application
#'

library(shiny)
library(shinyjs)

initval <<- 4

ui <- fluidPage(

  useShinyjs(), # <---

  tags$head(tags$style(type='text/css', 'body {background-color: #272b30; color: lightgray;}')),

  mainPanel(
    tags$h3('HTML5 localStorage'),
    uiOutput('sldr1')
  )
)

server <- function(input, output, session) {
  loaded <- FALSE

  output$sldr1 <- renderUI({sliderInput('slider1', 'Slider1', min = 1, max = 10, value = initval)})

  observeEvent(input$slider1, {
    print(paste('loaded', loaded))

    if (!loaded) {
      print('localStorage.getItem')

      shinyjs::runjs('if (localStorage) {
                        Shiny.onInputChange("slider1", localStorage.getItem("slider1"));
                      }')
      loaded <<- TRUE
      return()
    }

    print('updateSliderInput')
    isolate(updateSliderInput(session, 'slider1', value = input$slider1))

    if (loaded) {
      cmd = paste0('localStorage.setItem("', 'slider1', '", "', isolate(input$slider1), '");')

      print(cmd)

      shinyjs::runjs(cmd)
    }
  })
}

shinyApp(ui, server)
